#!/usr/bin/env nextflow

include {Multifetch} from './multifetch.nf'
nextflow.enable.dsl = 2

/*
 * Default pipeline parameters. They can be overridden on the command line eg.
 * given `params.foo` specify on the run command line `--foo some_value`.
 */

//Help
params.help = false

log.info """
Multifetch
${workflow.revision ?: 'master'} [$workflow.commitId]
===================================
"""

workflow {
  if (params.help) {
      log.info """
  Usage: 'nextflow run http://gitlab.com/uit-sfb/multifetch [-r <x.y.z> | -latest] [-nextflow-options ...] [--parameters]

  Common nextflow options:
    -r                                        Specify which version of metapipe to run (the list of releases can be found here: https://gitlab.com/uit-sfb/metapipe/-/releases).
                                              A specific (full) git hash from branch 'master' can alternatively be used.
    -latest                                   Pull the latest commit of branch 'master'. Alternative to '-r'.
                                              May not work if '-r' has been use in the past (deleting '.nextflow' directory fixes this issue).
    -resume                                   Use cached results and only reprocess tasks that previously failed or whose parameters changed in some way.
    -params-file <path/to/param.yml|.json>    Use parameters stored as key-value pairs in a Json/Yaml file.
                                              Remember to remove the '--' on the key side.
    -c <path/to/configFile>                   Provide config file (overlayed on top of the default one).
    -with-trace <trace.tsv>                   Generate a tsv file 'trace.tsv'
    -N <email_address>                        Send an email when workflow execution ends
    Find more nextflow options by running 'nextflow run -h'

  Multifetch settings:
    General:
      --help                                      Display help message
  """
  } else {
    Multifetch()
  }
}

 /*
  * completion handler
  */
workflow.onComplete {
  if (!params.help)
    workflow.success ? log.info("\nDone") : log.warn("\nOops .. something went wrong")
}
