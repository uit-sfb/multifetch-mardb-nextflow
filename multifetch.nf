include {downloadFromFTP} from './downloadFromFTP.nf'


workflow Multifetch {

  inputCh = Channel.fromPath(params.input)
    .splitCsv(sep: '\t', strip: true, skip: 1) //skip column name
    .map {row -> row[42]} //acc:genbank_accession column

  downloadFromFTP(inputCh)

}
