process downloadFromFTP {

    errorStrategy 'ignore'

    maxForks 2 //https://www.biostars.org/p/233899/ cannot run generate_urls in parallel due to Entrez not being able to handle multiple requests
    tag "$acc"
    echo true

    publishDir "${params.sequences}", mode: 'move'

    input:
      val(acc)

    output:
      path("${acc}")


    shell:
        '''
        #!/usr/bin/env python3

        import sys
        from Bio import Entrez
        sys.path.insert(0, "/app")
        import utils
        import downloadFromFtp
        import argparse
        import os
        import re
        import shutil
        from pathlib import Path

        cwd = os.getcwd()

        Entrez.email = "!{params.email}"
        Entrez.api = "!{params.api_key}"
        urls = downloadFromFtp.generate_urls("!{acc}","!{params.sequences}")
        downloadFromFtp.fetch_data(urls,cwd)

        '''
}
